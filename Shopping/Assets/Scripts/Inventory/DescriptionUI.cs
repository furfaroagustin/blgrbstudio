using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class DescriptionUI : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI _header;
    [SerializeField] TextMeshProUGUI _content;
    public int characterWraptLimit;
    [SerializeField] LayoutElement layoutElement;
    RectTransform _rect;
    private void Awake()
    {
        _rect = GetComponent<RectTransform>();
    }
    public void Show(Item item)
    {
        _header.text = item._itemData._name;
        _content.text = item._itemData._description;
        int headerLenght = _header.text.Length;
        int contentLenght = _content.text.Length;
        layoutElement.enabled = (headerLenght > characterWraptLimit || contentLenght > characterWraptLimit);
    }
    private void Update()
    {
        Vector2 position = Input.mousePosition;
        float pivotX = position.x / Screen.width;
        float pivotY = position.y / Screen.height;

        float finalPivotX;
        float finalPivotY;

        if (pivotX < 0.5)
        {
            finalPivotX = -0.1f;
        }
        else
        {
            finalPivotX = 1.01f;
        }
        if (pivotY < 0.5)
        {
            finalPivotY = 0f;
        }
        else
        {
            finalPivotY = 1f;
        }
        _rect.pivot = new Vector2(finalPivotX, finalPivotY);
        transform.position = position;
    }
}
