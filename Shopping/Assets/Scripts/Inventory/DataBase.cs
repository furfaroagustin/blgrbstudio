using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="DataBased",menuName ="Inventory/List",order =1)]
public class DataBase :ScriptableObject
{
   [System.Serializable]
   public struct InventoryItem
   {//estructura del objeto
        public string _name;
        public int _ID;
        public Sprite _icon;
        public TypeVar _type;
        public bool _acum;
        public int _maskStack;
        public string _description;
        public BaseItem item;
   }
    public enum TypeVar
    {
        _consumable,
        _equippable,
    }
    public InventoryItem[] _dataBase;
    private void OnValidate()
    {
        for (int i = 0; i < _dataBase.Length; i++)
        {
            if(_dataBase[i]._ID!=i)
            {
                _dataBase[i]._ID = i;    
            }
        }
    }
}
