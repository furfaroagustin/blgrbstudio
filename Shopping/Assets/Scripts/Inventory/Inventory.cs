using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.EventSystems;
public class Inventory : MonoBehaviour
{
    public GraphicRaycaster graphRay;
    public DataBase db;
    public int slotCount = 16;
   /* [ReadOnly]*/ public bool isOpen;
    [SerializeField]
    private PlayerController player;
    [SerializeField]
    private GameObject inventoryToogle;
    [SerializeField]
    private Transform slotPrefab;
    [SerializeField]
    private Transform itemPrefab;
    public DeletionPrompt deletionPrompt;
    public DescriptionUI descriptionUI;
    [SerializeField]
    private List<Item> items = new List<Item>();
    [SerializeField]
    private Transform slotConteiner;
    private List<Transform> slot = new List<Transform>();
    
    [SerializeField]
    public bool itemsDeleteModeEnable;
    public static Inventory _Instance { get; private set; }
    private void Awake()
    {
        if(_Instance!=null&&_Instance!=this)
        {
            Destroy(this);
        }
        else
        {
            _Instance = this;
        }
    }
    private void Start()
    {
        for(int i =0;i<slotCount;i++)
        {
            Transform newSlot = Instantiate(slotPrefab, slotConteiner);
            slot.Add(newSlot);
        }
        isOpen = true;
        ToogleInventory();
       
        
    }
    public void UpdateParent(Item item, Transform newParent)
    {
        item._exparent = newParent;
        item.transform.SetParent(newParent);
        item.transform.parent.GetComponent<Image>().fillCenter = true;
        item.transform.localPosition = Vector3.zero;
        item.EnableDeletion(itemsDeleteModeEnable);
    }
    public void DeleteItem(Item item, int quantity, bool byUse)
    {
        Item itemToDelete = items.Find(it => it == item);
        itemToDelete._quantity -= quantity;
        if(!byUse)
        {
            Debug.Log(item._itemData._name);
            BaseItem spawnedItem = Instantiate(item._itemData.item);
            spawnedItem.transform.position = player._itemSpawn.position;
           // spawnedItem.SetDataById(item._id, quantity);

        }
        if(itemToDelete._quantity<=0)
        {
            itemToDelete._exparent.GetComponent<Image>().fillCenter = false;
            //item.Remove(itemToDelete);
            Destroy(itemToDelete.gameObject);
        }
    }
    public void  ToogleDeleteInventory()
    {
        itemsDeleteModeEnable = !itemsDeleteModeEnable;
        foreach(Item item in items)
        {
            item.EnableDeletion(itemsDeleteModeEnable);

        }
    }
    public void ToogleInventory()
    {
        if(isOpen&&itemsDeleteModeEnable)
        {
            ToogleDeleteInventory();

        }
        GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
        inventoryToogle.SetActive(!isOpen);
        isOpen = !isOpen;
    }
    public void ShowDescription(Item item)
    {
        descriptionUI.gameObject.SetActive(true);
        descriptionUI.Show(item);
         
    }
    public void HideDescription()
    {
        descriptionUI.gameObject.SetActive(false);
    }
    public void ShowDeletionPropmt(Item item)
    {
        deletionPrompt.gameObject.SetActive(true);
        deletionPrompt.SetSliderData(item);
    }
    public void AdddMoreSpace(int slotsToAdd)
    {
        for(int i=0;i<slotsToAdd;i++)
        {
            Transform newSlot = Instantiate(slotPrefab, slotConteiner);
            slot.Add(newSlot);
        }
    }
    public void AddItem(int id, int quantity)
    {
        Item preexistentValidItem = items.Find(item => item._id == id && item._itemData._maskStack >= item._quantity + quantity);
        if(preexistentValidItem!=null)
        {
            preexistentValidItem._quantity += quantity;
            return;
        }
        for(int i=0;i<slot.Count;i++)
        {
            Item itemInSlot = slot[i].childCount == 0 ? null : slot[i].GetChild(0).GetComponent<Item>();
            if(itemInSlot==null)
            {
                Item itemCopy = Instantiate(itemPrefab, transform).GetComponent<Item>();
                itemCopy.InitializeItem(id, quantity);
                items.Add(itemCopy);
                UpdateParent(itemCopy, slot[i]);
                break;
            }
            else if(itemInSlot._id==id&&itemInSlot._itemData._maskStack>=itemInSlot._quantity+quantity)
            {
                itemInSlot._quantity += quantity;
                break;
            }
        }
    }
}