using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class Item : MonoBehaviour, IPointerClickHandler,IPointerEnterHandler,IPointerExitHandler,IBeginDragHandler,IDragHandler,IEndDragHandler
{
    [SerializeField]
    private DataBase _db;
    [SerializeField]
    private GameObject _deletButton;
    public int _id;
    public int _quantity;
    [HideInInspector]
    public DataBase.InventoryItem _itemData;
    [HideInInspector]
    public Transform _exparent;
    TextMeshProUGUI _quantityText;
    Image _IconImage;
    Vector3 _DragOffset;


    public void Awake()
    {
        _quantityText = transform.GetComponentInChildren<TextMeshProUGUI>();
        _IconImage = GetComponent<Image>();
        _exparent = transform.parent;
        if(_exparent.GetComponent<Image>())
        {
            _exparent.GetComponent<Image>().fillCenter = true;
        }
        InitializeItem(_id, _quantity);
    }

    public void Update()
    {
        if(_quantityText!=null)
        {
            _quantityText.text = _quantity.ToString();
        }
    }
    public void Delete()
    {
        Inventory._Instance.HideDescription();
        if(_quantity>1)
        {
            Inventory._Instance.ShowDeletionPropmt(this);

        }
        else
        {
            Inventory._Instance.DeleteItem(this,1,false);

        }
    }
    public  void InitializeItem(int _id,int _quantity)
    {
        _itemData._ID = _id;
        _itemData._acum = _db._dataBase[_id]._acum;
        _itemData._description = _db._dataBase[_id]._description;
        _itemData._icon = _db._dataBase[_id]._icon;
        _itemData._type = _db._dataBase[_id]._type;
        _itemData._maskStack = _db._dataBase[_id]._maskStack;
        _itemData.item = _db._dataBase[_id].item;
        _deletButton.SetActive(false);
        _IconImage.sprite = _itemData._icon;
        this._quantity = _quantity;
    }
    public void EnableDeletion(bool enable)
    {
        _deletButton.SetActive(enabled);
    }
    public void OnPointerEnter(PointerEventData eventData)
    {
        if(!eventData.dragging)
        {
            Inventory._Instance.ShowDescription(this);
        }
    }
    public void OnPointerClick(PointerEventData eventData)
    {
        if(eventData.clickCount==2)
        {
            Inventory._Instance.HideDescription();
            _itemData.item.Use();
            Inventory._Instance.DeleteItem(this, 1, true);
        }
    }
    public void OnPointerExit(PointerEventData eventData)
    {
        Inventory._Instance.HideDescription();
    }
    public void OnBeginDrag(PointerEventData eventData)
    {
      //   Inventory._Instance.HideDescription();
        _quantityText.enabled = false;
        _exparent = transform.parent;
        _exparent.GetComponent<Image>().fillCenter = false;
        transform.SetParent(Inventory._Instance.transform);
        _DragOffset = transform.position - Input.mousePosition;

    }
    public void OnDrag(PointerEventData eventData)
    {
        transform.position = Input.mousePosition + _DragOffset;
    }
    public void OnEndDrag(PointerEventData eventData)
    {
        _quantityText.enabled = true;
        List<RaycastResult> raycastResults = new List<RaycastResult>();
        Transform slot = null;
        Inventory._Instance.graphRay.Raycast(eventData, raycastResults);
        foreach(RaycastResult hit in raycastResults)
        {
            var hitobj = hit.gameObject;
            if (hitobj.CompareTag("Slot") && hit.gameObject.transform.childCount == 0)
            {
                slot = hit.gameObject.transform;
                break;
            }
            if(hitobj.CompareTag("Item"))
            {
                if(hitobj!= this.gameObject)
                {
                    Item hitObjItemData = hitobj.GetComponent<Item>();
                    if(hitObjItemData._itemData._ID!=_id)
                    {
                        slot = hitObjItemData.transform.parent;
                        Inventory._Instance.UpdateParent(hitObjItemData, _exparent);
                        break;
                    }
                    else
                    {
                        if(_itemData._acum && hitObjItemData._quantity+_quantity<=_itemData._maskStack)
                        {
                            _quantity += hitObjItemData._quantity;
                            slot = hitObjItemData.transform.parent;
                            Inventory._Instance.DeleteItem(hitObjItemData, hitObjItemData._quantity, true);
                            break;

                        }
                        else
                        {
                            slot = hitObjItemData.transform.parent;
                            Inventory._Instance.UpdateParent(hitObjItemData, _exparent);
                            break;
                        }
                    }
                }
            }
        }
        Inventory._Instance.UpdateParent(this, slot!=null?slot:_exparent);


    }
    
}
