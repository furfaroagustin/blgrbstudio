using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TMPro;
using UnityEngine.SceneManagement;


public class NpcShop : MonoBehaviour
{
    public GameObject interactionText;
    public GameObject dialoguePanel;
    public TextMeshProUGUI dialogueText;
   
    public GameObject canvas;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("NPC"))
        {
            interactionText.SetActive(true);
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("NPC"))
        {
            interactionText.SetActive(false);
            dialoguePanel.SetActive(false);
            
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.X) && interactionText.activeSelf)
        {
            interactionText.SetActive(false);
            dialoguePanel.SetActive(true);
           
            dialogueText.text = "Hello, I am the merchant, do you want to buy or sell something?";
        }
    }

    public void ExitDialogue()
    {
        dialoguePanel.SetActive(false);
        
    }

    public void BuyButton()
    {
        //SceneManager.LoadScene("Store");
        canvas.SetActive(true);
    }
    public void ExitCanvas()
    {
        //SceneManager.LoadScene("Store");
        canvas.SetActive(false);
        dialoguePanel.SetActive(false);
        
    }

    public void SellButton()
    {
        //SceneManager.LoadScene("StoreSell");
    }
}
