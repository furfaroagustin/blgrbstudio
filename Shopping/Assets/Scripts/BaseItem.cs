using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public abstract class BaseItem : MonoBehaviour
{
    public int id;
    public int quantity = 1;
    /*[ReadOnly]*/
    public DataBase.InventoryItem itemData;
    private void Start()
    {
        SetDataById(id, quantity);
    }
    public void SetDataById(int id,int quantity=1)
    {
       
        itemData._ID = id;
        itemData._acum = Inventory._Instance.db._dataBase[id]._acum;
        itemData._description = Inventory._Instance.db._dataBase[id]._description;
        itemData._icon = Inventory._Instance.db._dataBase[id]._icon;
        itemData._name = Inventory._Instance.db._dataBase[id]._name;
        itemData._type = Inventory._Instance.db._dataBase[id]._type;
        itemData._maskStack = Inventory._Instance.db._dataBase[id]._maskStack;
        itemData.item = Inventory._Instance.db._dataBase[id].item;
        this.quantity = quantity;
    }
    public abstract void Use();
    public void OnTriggerEnter2D(Collider2D other)
    {
        if(other.transform.CompareTag("Player"))
        {
            Inventory._Instance.AddItem(id, quantity);
            Destroy(this.gameObject);
        }
    }
}
