using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed;
    public float delay;
    private float[] walking;
    private Animator ani;
    private RaycastHit2D hit;
    public LayerMask obstacle;
    private Vector3 dir;
    public Vector3 v3;
    private Vector3 pose;
    public bool movementHab = true;
    public GameObject panel;
    Inventory inventory;
    public Transform _itemSpawn;


    private void Start()
    {
        dir = new Vector2(0, -1);
        ani = GetComponent<Animator>();
        walking = new float[4];
        pose = transform.position;
        inventory=Inventory._Instance;
    }

    bool CheckCollision
    {
        get
        {
            hit = Physics2D.Raycast(transform.position + v3, dir, 1, obstacle);
            return hit.collider != null;

        }
    }
    private void OnDrawGizmos()
    {
        Gizmos.DrawRay(transform.position + v3, dir);
    }

    public void Move_()
    {
        if (Input.GetKey(KeyCode.W))
        {
            dir = new Vector2(0, 1);
            walking[0] += 1 * Time.deltaTime;
            if (transform.position == pose)
            {
                ani.SetFloat("movX", 0);
                ani.SetFloat("movY", 1);
                if (!CheckCollision && walking[0] > delay)
                {
                    ani.SetBool("Walk", true);
                    pose += dir;
                }

            }
        }
        else
        {
            walking[0] = 0;
        }
        if (Input.GetKey(KeyCode.S))
        {
            dir = new Vector2(0, -1);
            walking[1] += 1 * Time.deltaTime;
            if (transform.position == pose)
            {
                ani.SetFloat("movX", 0);
                ani.SetFloat("movY", -1);
                if (!CheckCollision && walking[1] > delay)
                {
                    ani.SetBool("Walk", true);
                    pose += dir;
                }

            }
        }
        else
        {
            walking[1] = 0;
        }
        if (Input.GetKey(KeyCode.D))
        {
            dir = new Vector2(1, 0);
            walking[2] += 1 * Time.deltaTime;
            if (transform.position == pose)
            {
                ani.SetFloat("movX", 1);
                ani.SetFloat("movY", 0);
                if (!CheckCollision && walking[2] > delay)
                {
                    ani.SetBool("Walk", true);
                    pose += dir;
                }

            }
        }
        else
        {
            walking[2] = 0;
        }
        if (Input.GetKey(KeyCode.A))
        {
            dir = new Vector2(-1, 0);
            walking[3] += 1 * Time.deltaTime;
            if (transform.position == pose)
            {
                ani.SetFloat("movX", -1);
                ani.SetFloat("movY", 0);
                if (!CheckCollision && walking[3] > delay)
                {
                    ani.SetBool("Walk", true);
                    pose += dir;
                }

            }
        }
        else
        {
            walking[3] = 0;
        }
        if (transform.position == pose)
        {
            ani.SetBool("Walk", false);
        }
        transform.position = Vector3.MoveTowards(transform.position, pose, speed * Time.deltaTime);
    }
    private void Update()
    {
        if (movementHab)
        {
            Move_();
            if (Input.GetKeyDown(KeyCode.Space))
            {
                inventory.ToogleInventory();
                panel.SetActive(!panel.activeSelf);
            }
        }

    }
    public void DetenerMovimiento()
    {
        // Detener el movimiento del jugador
        movementHab = false;
    }

    public void IniciarMovimiento()
    {
        // Permitir que el jugador se mueva nuevamente
        movementHab = true;
    }
}